
import React, { Component } from 'react'
import ReactMarkdown from 'react-markdown'

class ReadmeShower extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      readmeFile: null,
      readmePath : require("../README.md")
    }
  }

  componentWillMount() {
    fetch(this.state.readmePath).then((response) => response.text()).then((text) => {
      this.setState({ readmeFile: text })
    })
  }

  render() {
    return (
      <div className="content">
        <ReactMarkdown source={this.state.readmeFile} />
      </div>
    )
  }
}

export default ReadmeShower