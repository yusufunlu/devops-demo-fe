import React, { Component } from 'react';
const API = 'http://localhost:8080/';
const DEFAULT_QUERY = 'task';
class FetchComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
    };
  }
  componentDidMount() {
    fetch(API + DEFAULT_QUERY)
      .then(response => response.json())
      .then(data => this.setState({ tasks: data }));
  }
  render() {
    const { tasks } = this.state;
    return (
        !this.state.tasks ? <div>Task yoktur </div> : 
      <ul>
        {tasks.map(task =>
          <li key={task.objectID}>
            <div>{task.title}</div>
            <div>{task.content} </div>
          </li>
        )}
      </ul>
    );
  }
}
export default FetchComp;