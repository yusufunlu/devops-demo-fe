import React from 'react';
import '../styles/App.css';
import FetchComp from './FetchComp'
import ReadmeShower from './ReadmeShower'

function App() {
  return (
    <div className="App">
      <FetchComp/>
      <ReadmeShower/>
    </div>
  );
}

export default App;
