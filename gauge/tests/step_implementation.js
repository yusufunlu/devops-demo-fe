/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, text, focus, textBox, toRightOf } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({
        headless: true,
        args: ["--no-sandbox", "--disable-setuid-sandbox"]
      });
});

/*afterSuite(async () => {
    await closeBrowser();
});*/

step("Open the page", async () => {
    await goto("http://146.148.32.57");
});

step("Page Contains <todoItem>", async (todoItem) => {
    await text(todoItem).exists();
});